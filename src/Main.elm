import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http exposing (..)
import Json.Decode as Decode
import Multiselect
import Bootstrap.CDN as CDN
import Bootstrap.Grid as Grid
import Bootstrap.Form.InputGroup as InputGroup
import Bootstrap.Form.Input as Input
import Bootstrap.Navbar as Navbar
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Button as Button
import Bootstrap.ListGroup as Listgroup
import Bootstrap.Modal as Modal
import Bootstrap.Utilities.Border as Border
import Random.List exposing (choose)
import Random
import Array



--API

apiUrl: String
apiUrl = "https://api.edamam.com"
apiKey: String
apiKey = "03b4e4c4dfb5f5d9739d044eab8d8e7c"
apiId: String
apiId = "f06ce80e"

myViewFn =
    Grid.row [ Row.centerXs ]
        [ Grid.col [ Col.xs2 ]
            [ text "Col 1" ]
        , Grid.col [ Col.xs4 ]
            [ text "Col 2" ]
        ]



type alias Recipe =
  {
    url: String,
    label: String,
    image: String, 
    yield: Float,
    dietLabels: List String,
    healthLabels: List String,
    ingredients: List Ingredients,
    nutrients: Nutrients
  }

type alias Nutrients = 
  {
    calories: Nutrient,
    fat: Nutrient,
    carbs: Nutrient,
    fiber: Nutrient,
    protein: Nutrient,
    sugar: Nutrient
  }

type alias Nutrient =
  {
    label: String,
    quantity: Float,
    unit: String
  }

type alias Ingredients =
  {
    text: String,
    weight: Float
  }

type alias UserInput = {
  calories: Float 
  ,weight: Float
  ,height: Float
  ,ingredient: String
  ,diets: Multiselect.Model
  ,healthLabels: Multiselect.Model
  ,mealType: Multiselect.Model
  }

excludedFoods : List (String, String)
excludedFoods=
  [ ( "0","egg")
  , ( "1","peanut")
  , ( "2","nuts")
  , ( "3","milk")
  , ( "4","gluten")
  , ( "5","shellfish")
  ]

diet : List(String, String)
diet = 
  [ ( "0","balanced")
  , ( "1","high-fiber")
  , ( "2","high-protein")
  , ( "3","low-carb")
  , ( "4","low-fat")
  , ( "5","low-sodium")
  ]

healthLabel : List (String, String) -- add values
healthLabel =
  [ ( "0","alcohol-free")
  , ( "1","kidney-friendly")
  , ( "2","kosher")
  , ( "3","low-sugar")
  , ( "4","paleo")
  , ( "5","pescatarian")
  , ( "6","vegan")
  , ( "7","vegetarian")
  , ( "8","low-carb")
  , ( "9","celery-free")
  , ( "10","crustacean-free")
  , ( "11","dairy-free")
  , ( "12","egg-free")
  , ( "13","gluten-free")
  , ( "14","low-potassium")
  , ( "15","lupine-free")
  , ( "16","mustard-free")
  , ( "17","No-oil-added")
  , ( "18","peanut-free")
  , ( "19","sesame-free")
  , ( "20","shellfish-free")
  , ( "21","soy-free")
  , ( "22","tree-nut-free")
  , ( "23","wheat-free ")
  ]

mealTypes : List (String, String) -- add values
mealTypes =
  [ ( "0","breakfast")
  , ( "1","lunch")
  , ( "2","snack")
  , ( "3","dinner")
  ]

--userInput: UserInput
--userInput = UserInput 0 0 0 0
-- MAIN
-- for testing API output: curl "https://api.edamam.com/search?q=chicken&app_id=f06ce80e&app_key=03b4e4c4dfb5f5d9739d044eab8d8e7c&from=0&to=3&calories=591-722&health=alcohol-free"
main =
  
  Browser.element
    { init = init
    , update = update
    , subscriptions = subscriptions
    , view = view
    }
randomFood: List String
randomFood = ["chicken","beef","pork", "fish","sausage","salami","ham","egg","grains","cereal",
  "bagel","pancakes","avocado","potatoe","rice","bread","beans", "pasta","apple","pear","banana",
  "mango","bacon","hotdog","blueberrie","barley","spelt","quinoa","pumpkin"] 

initialRecipe = Recipe "https://www.seriouseats.com/recipes/2012/08/grilled-butterflied-chicken-recipe.html" "Grilled Butterflied Chicken Recipe" 
  "https://www.edamam.com/web-img/7a2/7a2f41a7891e8a8f8a087a96930c6463.jpg" 4 ["Low-Carb"] ["Sugar-Conscious, Peanut-Free, Tree-Nut-Free, Alcohol-Free "] 
  [Ingredients "1 whole chicken, 3 1/2 to 4 pounds" 1700.9713875, Ingredients "Kosher salt and freshly ground black pepper" 10.205828325] 
  (Nutrients (Nutrient "Energy" 2499.628483072875 "kcal") (Nutrient "Fat" 174.35943285279748 "g") (Nutrient "Carbs" 3.2633136069187505 "g") 
  (Nutrient "Fiber" 1.2910372831125 "g") (Nutrient "Protein" 215.66905387248374 "g") (Nutrient "Sugars" 0.03265865064 "g") ) -- for testing
-- MODEL


type alias Model = 
  {
    recipe: Recipe
    , userInput: UserInput
    , success: Bool
  }

model_initial: Model
model_initial = 
  {
    recipe = initialRecipe
    , userInput = UserInput 0 0 0 "" (Multiselect.initModel diet "") (Multiselect.initModel healthLabel "") (Multiselect.initModel mealTypes "lunch")
    , success = False
  }


init : () -> (Model, Cmd Msg)
init _ =
  (model_initial, Cmd.none)



-- UPDATE


type Msg
  = MorePlease
  | APILoaded (Result Http.Error (List Recipe))
  | UpdateCalories String
  | UpdateWeight String
  | UpdateHeight String
  | UpdateIngredient String
  | UpdateDiets Multiselect.Msg
  | UpdateHealthLabels Multiselect.Msg
  | UpdateMealType Multiselect.Msg
  | RandomIngredient Int


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    MorePlease -> --generater random ingredient if not provided and calls getRecipe
      case model.userInput.ingredient of
        "" -> (model, Random.generate RandomIngredient (Random.int 0 (List.length randomFood - 1)))
        _ -> (model, getRecipe model)


    APILoaded result ->
      let
        oldUserInput = model.userInput
        newUserInput = {oldUserInput| ingredient = ""}
      in
      case result of
        
        Ok data->
          ({model| recipe = (Maybe.withDefault initialRecipe (List.head data)), success = True, userInput = newUserInput} , Cmd.none)

        Err _ ->
          ({model| success = False, userInput = newUserInput}, Cmd.none)
    
    UpdateIngredient input -> 
        let
          oldUserInput = model.userInput
          newUserInput = {oldUserInput| ingredient = input}
        in
        
        ({model| userInput = newUserInput} , Cmd.none)
    
    UpdateCalories input -> 
        let
          oldUserInput = model.userInput
          newUserInput = {oldUserInput| calories = Maybe.withDefault 0 (String.toFloat(input))}
        in
        
        ({model| userInput = newUserInput} , Cmd.none)
    
    UpdateHeight input -> 
        let
          oldUserInput = model.userInput
          newUserInput = {oldUserInput| height = Maybe.withDefault 0 (String.toFloat(input))}
        in
        
        ({model| userInput = newUserInput} , Cmd.none)
    
    UpdateWeight input -> 
        let
          oldUserInput = model.userInput
          newUserInput = {oldUserInput| weight = Maybe.withDefault 0 (String.toFloat(input))}
        in
        
        ({model| userInput = newUserInput} , Cmd.none)
    
    UpdateDiets sub ->
      let
        ( subModel, subCmd, _ ) =
          Multiselect.update sub model.userInput.diets
        oldUserInput = model.userInput
        newUserInput = {oldUserInput| diets = subModel}
      in
      ({ model | userInput = newUserInput }, Cmd.map UpdateDiets subCmd )

    UpdateHealthLabels sub ->
      let
        ( subModel, subCmd, _ ) =
          Multiselect.update sub model.userInput.healthLabels
        oldUserInput = model.userInput
        newUserInput = {oldUserInput| healthLabels = subModel}
      in
      ({ model | userInput = newUserInput }, Cmd.map UpdateHealthLabels subCmd )

    UpdateMealType sub ->
      let
        ( subModel, subCmd, _ ) =
          Multiselect.update sub model.userInput.mealType
        oldUserInput = model.userInput
        newUserInput = {oldUserInput| mealType = subModel}
      in
      ({ model | userInput = newUserInput }, Cmd.map UpdateMealType subCmd )

    RandomIngredient number ->
        let
          oldUserInput = model.userInput
          result = Maybe.withDefault "chicken" (Array.fromList randomFood |> Array.get number)
          newUserInput = {oldUserInput| ingredient = result}
        in
        ({model| userInput = newUserInput} , getRecipe {model| userInput = newUserInput})

-- SUBSCRIPTION
subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.batch
    [ Sub.map UpdateHealthLabels <| Multiselect.subscriptions model.userInput.healthLabels
     , Sub.map UpdateDiets <| Multiselect.subscriptions model.userInput.diets
     , Sub.map UpdateMealType <| Multiselect.subscriptions model.userInput.mealType
    ]

-- VIEW

header : Html msg
header =
    Html.header []
        [ div [ id "header-image" ]
            [ img [ src "fb1.png" ]
                []
            ]
        , div [ id "header-name" ]
            [ text "FOODBUDDY" ]
        ]

view : Model -> Html Msg
view model =
  Grid.container []
    [
     CDN.stylesheet
    , viewRecipe model
    ]


ingredients: Ingredients -> String
ingredients i=
  let
      w=String.fromInt(round i.weight)
      n=i.text
      s=n++": "++w++" g"
  in
    s
  

ingredientsMap: List Ingredients -> List String
ingredientsMap l=
  List.map ingredients l

toHtmlList : List String -> Html msg
toHtmlList strings =
  ul [] (List.map toLi strings)

toLi : String -> Html msg
toLi s = 
  li [] [ text s ]

viewRecipe : Model -> Html Msg
viewRecipe model =

      div []
        [h1[style "text-align" "center"][text("Food Buddy")],hr[][]
        ,br[][]
        ,Grid.row [][
          Grid.col [][div [style "text-align" "center" ][img [ src model.recipe.image ] []]]
          ,Grid.col [][ div[Border.all,Border.rounded,Border.dark][
                        h2[style "text-align" "center", style "bold" "bold"][text(model.recipe.label)]
                        ,h3[][text("\n")]
                        ,h3[][text("\n")]
                        ,h3[][text("\n")]
                        ,h4[style "text-align" "center"][text ("Nutrients per serving:")]
                      , ul [][
                        li[][text ("Energy: " ++ String.fromInt( round (model.recipe.nutrients.calories.quantity/model.recipe.yield)) ++ " " ++ model.recipe.nutrients.calories.unit )]
                      , li[][text ("Fat: " ++ String.fromInt( round (model.recipe.nutrients.fat.quantity/model.recipe.yield)) ++ " " ++ model.recipe.nutrients.fat.unit )]
                      , li[][text ("Protein: " ++ String.fromInt( round (model.recipe.nutrients.protein.quantity/model.recipe.yield)) ++ " " ++ model.recipe.nutrients.protein.unit )]
                      , li[][text ("Carbs: " ++ String.fromInt( round (model.recipe.nutrients.carbs.quantity/model.recipe.yield)) ++ " " ++ model.recipe.nutrients.carbs.unit )]
                      , li[][text ("Sugars: " ++ String.fromInt( round (model.recipe.nutrients.sugar.quantity/model.recipe.yield)) ++ " " ++ model.recipe.nutrients.sugar.unit )]
                      , li[][text ("Fiber: " ++ String.fromInt( round (model.recipe.nutrients.fiber.quantity/model.recipe.yield)) ++ " " ++ model.recipe.nutrients.fiber.unit )]]
          ]]
          ,Grid.col[][div[Border.all,Border.rounded,Border.dark][
                      h3[style "text-align" "center"][text("Ingredients")]
                      ,toHtmlList (ingredientsMap model.recipe.ingredients)
                      ],br[][]
                      ,div[style "text-align" "center"][Button.linkButton [Button.success, Button.attrs [ href model.recipe.url] ] [ text "Recipe" ]]]
        ]
        ,br[][]
        ,Grid.row[][
            Grid.col[][div[Border.all,Border.rounded,Border.dark][
              p[][text (" Generate recipe based on desired caloric input.")]
              ,InputGroup.config
              (InputGroup.text [ Input.placeholder "kcal"])
              |> InputGroup.predecessors
                  [ InputGroup.span [] [ text "Calories"] ]
              |> InputGroup.attrs [onInput UpdateCalories ]
              |> InputGroup.view
          ] ,div[][br[][],br[][],br[][],br[][],br[][],br[][],br[][],br[][],br[][],img[src "white.png"][]]
          
          ]

          ,Grid.col[][div[Border.all,Border.rounded,Border.dark][
            p[][text (" Generate recipe based on your BMI.")]
            ,InputGroup.config
            (InputGroup.text [ Input.placeholder "kg"])
            |> InputGroup.predecessors
                [ InputGroup.span [] [ text "Weight"] ]
            |> InputGroup.attrs [onInput UpdateWeight ]    
            |> InputGroup.view
        , InputGroup.config
            (InputGroup.text [ Input.placeholder "cm"])
            |> InputGroup.predecessors
                [ InputGroup.span [] [ text "Height"] ]
            |> InputGroup.attrs [onInput UpdateHeight ]    
            |> InputGroup.view
          ],br[][]
          ,div[style "text-align" "center"][
            Button.button [Button.success, Button.attrs [onClick MorePlease]][text "Generate Recipe!"]
          ]]

        ,Grid.col[][div[Border.all,Border.rounded,Border.dark][
          p[][text("Enter prefered food:")]
          ,InputGroup.config
            (InputGroup.text [ Input.placeholder ""])
            |> InputGroup.predecessors
                [ InputGroup.span [] [ text "fish, salad, eggs... "] ]
            |> InputGroup.attrs [onInput UpdateIngredient, value model.userInput.ingredient ]    
            |> InputGroup.view
        ]
        , br [][]
        ,div[Border.all,Border.rounded,Border.dark][
         p[][text("Diet choice:")]
        , Html.map UpdateDiets <| Multiselect.view model.userInput.diets
        , p[][text("Health choices / allergies:")]
        , Html.map UpdateHealthLabels <| Multiselect.view model.userInput.healthLabels
        , p[][text("Meal type:")]
        , Html.map UpdateMealType <| Multiselect.view model.userInput.mealType
        ]]]
       
      
      
        ]


getRecipe : Model -> Cmd Msg
getRecipe input =
  Http.get
    { 
      url = generateAPIRequestUrl input,
      expect = Http.expectJson APILoaded recipesDecoder
      
    }


generateAPIRequestUrl : Model -> String
generateAPIRequestUrl model = 
  let
      input = model.userInput
  in
  apiUrl ++ "/search?" ++ "q=" ++ input.ingredient  ++ "&app_id=" ++ apiId ++ "&app_key=" ++ apiKey ++ "&calories=" ++ getCalories input ++ getDiets (Multiselect.getSelectedValues (input.diets)) ++ getHealthLabels (Multiselect.getSelectedValues (input.healthLabels))


getDiets : List (String, String) -> String -- transforms user diet input into API usable string
getDiets input = 
  let
    helper : List (String, String) -> String
    helper diets = 
      case diets of
        [] -> "" 
        [(x,y)] -> y
        (x,y):: tail -> y ++ "," ++getDiets tail
    result = helper input
  in
  if result /= ""then 
    "&diet=" ++helper input
  else
    ""

getHealthLabels : List (String, String) -> String -- transforms user healthLabel input into API usable string
getHealthLabels input = 
  let
    helper : List (String, String) -> String
    helper healthLabels = 
      case healthLabels of
        [] -> "" 
        [(x,y)] -> y
        (x,y):: tail -> y ++ "," ++ helper tail
    result = helper input
  in
  if result /= ""then 
    "&health=" ++ helper input
  else 
    ""


getCalories : UserInput -> String -- returns calories as String ready for API request (from-to)
getCalories input = 
  if input.weight > 0.1 && input.height > 0.1 && input.calories == 0 then -- if user provides weight and height and no calories calculate appropriate calories
    let
        bmi = calculateBMI input.weight input.height --calculates BMI
        calories = calculateCalories bmi (Tuple.second (Maybe.withDefault ("0", "lunch") ((List.head (Multiselect.getSelectedValues (input.mealType)))))) -- calculates calories based on BMI and mealType
    in
    String.fromInt(Tuple.first calories) ++ "-" ++ String.fromInt(Tuple.second calories) -- returns calories as String ready for 

  else if input.calories > 0.1 then -- if user provides calories
    String.fromFloat(input.calories) ++ "-" ++ String.fromFloat(input.calories + 200)

  else -- if no user input is provided
    "300-1000"

calculateCalories : String -> String -> (Int, Int) -- calculates calories based on BMI and mealType
calculateCalories bmi meal = 
  case bmi of 
    "underweight" -> 
      case meal of 
        "breakfast" -> (450, 600)
        "lunch" -> (600, 800)
        "snack" -> (300, 500)
        "dinner" -> (600,800)
        napaka -> (600, 800) -- cant get to here 
    "normal" -> 
      case meal of 
        "breakfast" -> (350, 500)
        "lunch" -> (500, 700)
        "snack" -> (200, 300)
        "dinner" -> (500, 700)
        napaka -> (600, 800) -- cant get to here 
    "overweight" ->
      case meal of 
        "breakfast" -> (300, 400)
        "lunch" -> (400, 600)
        "snack" -> (200, 300)
        "dinner" -> (300, 500)
        napaka -> (600, 800) -- cant get to here 
    "obese" -> 
      case meal of 
        "breakfast" -> (350, 500)
        "lunch" -> (500, 700)
        "snack" -> (200, 300)
        "dinner" -> (500, 700)
        napaka -> (600, 800) -- cant get to here 
    napaka -> (600, 800) -- cant get to here 
    
calculateBMI : Float -> Float -> String --calculates BMI
calculateBMI w h = --weight height age
  let
     bmi = w / ((h * 0.01)^2)
  in
  if bmi < 18.5 then "underweight"
  else if bmi >= 18.5 && bmi <= 24.9 then "normal"
  else if bmi > 24.9 && bmi <= 29.9 then "overweight"
  else "obese"

--DECODERS

recipesDecoder : Decode.Decoder (List Recipe)
recipesDecoder = 
  Decode.at ["hits"] (Decode.list recipeHelperDecoder)

recipeHelperDecoder: Decode.Decoder Recipe
recipeHelperDecoder = 
  Decode.at ["recipe"] recipeDecoder

recipeDecoder : Decode.Decoder Recipe
recipeDecoder = 
  Decode.map8
    Recipe
    (Decode.at [ "url" ] Decode.string)
    (Decode.at [ "label" ] Decode.string)
    (Decode.at [ "image" ] Decode.string)
    (Decode.at [ "yield" ] Decode.float)
    (Decode.at [ "dietLabels" ] (Decode.list Decode.string))
    (Decode.at [ "healthLabels" ] (Decode.list Decode.string))
    (Decode.at [ "ingredients" ] (Decode.list ingredientsDecoder))
    (Decode.at [ "totalNutrients" ] nutrientsDecoder)
   
ingredientsDecoder : Decode.Decoder Ingredients
ingredientsDecoder = 
  Decode.map2
    Ingredients
    (Decode.at ["text"] Decode.string)
    (Decode.at ["weight"] Decode.float)

nutrientsDecoder : Decode.Decoder Nutrients
nutrientsDecoder = 
    Decode.map6
      Nutrients
      (Decode.at ["ENERC_KCAL"] nutrientDecoder) -- calories
      (Decode.at ["FAT"] nutrientDecoder) -- fat
      (Decode.at ["CHOCDF"] nutrientDecoder) -- carbs
      (Decode.at ["FIBTG"] nutrientDecoder) -- fiber
      (Decode.at ["PROCNT"] nutrientDecoder) -- protein
      (Decode.at ["SUGAR"] nutrientDecoder) -- sugar

nutrientDecoder : Decode.Decoder Nutrient
nutrientDecoder = 
  Decode.map3
    Nutrient
    (Decode.at ["label"] Decode.string)
    (Decode.at ["quantity"] Decode.float)
    (Decode.at ["unit"] Decode.string)